// 定义一个类，用于创建vue的实例
class Vue {
  constructor(options = {}) {
    // 给实例增加属性
    this.$el = options.el
    this.$data = options.data
    this.$methods = options.methods
    // 对data的属性进行监视，也就是数据劫持
    new Observer(this.$data)
    // 把data中的数据代理到vm  这样就不用 vm.$data.car 直接vm.car
    this.proxy(this.$data)
    // methods代理到vm
    this.proxy(this.$methods)
    // 如果指定el参数，对el进行解析
    if (this.$el) {
      // compile复杂解析模板的内容
      // 需要 模板和数据，不如传整个vue实例
      const c = new Compile(this.$el, this)
      console.log(c);

    }
  }
  // 数据代理 也就是挂载 
  proxy(obj) {
    Object.keys(obj).forEach(key => {
      Object.defineProperty(this, key, {
        enumerable: true,
        configurable: true,
        get: () => {
          // 涉及到读取的操作
          return obj[key]
        },
        set: (newValue) => {
          if (obj[key] === newValue) {
            return
          }
          obj[key] = newValue
        }
      })
    })

  }


}