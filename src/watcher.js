/**
 * watch负责把compile模块和observer模块关联起来
 * 一旦vm的data的数据键名字发生变化的时候，就触发回调函数
 * 每一个键名字就会新建一个watcher。
 * watch的原理是，初次渲染的时候，也就是在compile那边就建立watch对象，传入cb，cb也就是重新渲染dom，这时候并不执行。
 * 但是watch对象已经存在了。此时一旦改变相应的值，也就是setter就会知道值变化了，在setter那边执行watch对象的update方法，update会执行cb
 * 这样compile就和observer联系起来。说的直白点。 一旦 data.msg = 9 触发setter ,执行update,执行cb。
 * 这里还有一个点。同一个数据比如msg,v-text的时候建立了一个watch,v-html的时候也建立一个watch,但其实msg变化的时候，两个watch的update都需要执行。
 * 于是，这里涉及到了订阅-发布模式。就是一旦值发生变化，就通知所有订阅该值的watch执行update.
 * 于是，每个值就是一个模式
 * 
 * vue那边，首先是数据劫持，也就是监视每个属性，给每个属性新建dep,然后是compile,compile就会new watch,
 * 这样就会把watch存到Dep.target，紧接着读取属性，这里就可以把watch放进对于的dep里。再清空target。
 *   Dep.target = this
    this.oldValue = this.getVMValue(vm,expr)
 */
class Watch{
  // vm是vm实例，expr是data的数据键名字，cb是这个名字发生变化的时候触发的回调函数。
  constructor(vm,expr,cb){
    this.vm = vm
    this.expr = expr
    this.cb = cb
    /**
     * this表示新创建的watch对象
     * 存储到Dep的target，Dep类只有一个，
     */
    Dep.target = this
    // 需要把expr的旧值存储起来,这样值发生变化的时候就可以执行cb
    // this.oldValue = this.vm.$data[expr]  后面这种处理不了 vm.data.car.color这种复杂数据
    this.oldValue = this.getVMValue(vm,expr)
    // 
    Dep.target = null

  }
  // 对外暴露的方法，用于更新页面，也就是让页面{{msg}}重新编译下，显示最新的属性值
  update(){
    // 首先看下值有没有变化，一旦变化调用cb
    let oldValue = this.oldValue
    // 首先创建watch实例的时候，存储最开始的值
    // 之后在访问的时候就算是新值了。
    let newValue = this.getVMValue(this.vm,this.expr)
    if(oldValue!=newValue){
      this.cb(newValue,oldValue)
    }
  }
  getVMValue(vm,expr){
    let value = vm.$data
    // car.color
    expr.split('.').forEach(item=>{
      value = value[item]
    })
    return value
   
  }
}
/**
 * dep是dependency缩写，表示依赖收集,收集所有订阅者
 * dep对象用于管理和通知所有的订阅者
 */
class Dep{
  constructor(){
    this.subs = []
  }
  // 添加
  addSub(watcher){
    this.subs.push(watcher)
  }
  // 通知
  notify(){
    this.subs.forEach(watcher=>{
      watcher.update()
    })
  }
}
/**
 * 1. 获取一个对象的键值，有时候不止是obj[key]这么简单
 * 比如 obj = {car:{color:'red'}}想得到最后的red,就必须obj['car']['color'],{{car.color}}
 * function getValue(obj,expr){
 *  let value = obj
 *  expr.split('.').forEach(key=>{
 *    // 迭代
 *     value = obj[key]
 *  })
 * return value
 * }
 * 
 * dep还是不是很明白。
 */