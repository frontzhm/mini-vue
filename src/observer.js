// Observer用于给data增加getter setter
class Observer {
  constructor(data) {
    console.log(data)
    this.data = data
    this.walk(data)
  }
  /**
   * 核心方法
   */
  // 遍历data的属性
  walk(obj) {
    // obj必须是存在的对象
    if (!obj || typeof obj !== 'object') {
      return
    }

    Object.keys(obj).forEach(key => {
      this.defineReactive(obj, key, obj[key])
      // 如果属性值是对象的话，需要再次遍历。
      this.walk(obj[key])
    })
  }
  // data中的每一个数据都应该维护一个dep对象，dep它保存所有订阅了该值的订阅者。
  // get收集watch,set触发watch
  // 响应式 也就是数据劫持
  defineReactive(obj, key, value) {
    let _this = this
    let dep = new Dep()
    Object.defineProperty(obj, key, {
      enumerable: true,
      configurable: true,
      get() {
        // 如果dep的target中有watch对象，存储到订阅者数组
        Dep.target && dep.addSub(Dep.target)
        console.log('dep')
        console.log(dep)
        console.log('get exec')
        return value
      },
      // 设置是为了下次获得值的时候 变化 
      set(newValue) {
        if (value === newValue) {
          return
        }
        console.log('set exec')
        value = newValue
        // 新值是对象的话也需要再次遍历。注意this取值
        _this.walk(newValue)
        // 值变化，就调用update，这样才能执行watch的cb，也就是重新渲染页面
        // window.watch.update()
        // 发布通知，让所有订阅者更新内容
        dep.notify()
      }
    })
  }
}
/**
 * 递归的反复使用，注意停止递归的条件
 */

 