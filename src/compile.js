/**
 * 专门负责解析模板内容
 * 参数1 ： 模板（就是dom对象啦）
 * 参数2： vue实例
 */

class Compile {
  constructor(el, vm) {
    // el就是new vue传的选择器
    // vm就是vue的实例
    // 考虑el就是dom对象的情况
    this.el = typeof el === 'string' ? document.querySelector(el) : el
    this.vm = vm
    console.log(this.el)
    // 编译模板
    if (this.el) {
      // 1. 把el中所有的子节点都放入到内存中，fragment（避免多次重绘和回流）[mdn搜下这个 一目了然]]
      let fragment = this.node2fragment(this.el)
      // 2. 在内存中编译fragment
      this.compile(fragment)
      // 3. 把fragment一次性插入到页面
      this.el.appendChild(fragment)

    }

  }
  /**
   * 核心方法(跟编译相关)
   */
  node2fragment(node) {
    let fragment = document.createDocumentFragment()
    // 把el所有的子节点挨个添加到fragment
    let childNodes = node.childNodes
    this.toArray(childNodes).forEach(node => {
      fragment.appendChild(node)
    })
    return fragment
  }
  compile(fragment) {
    /**
     * 编译子节点(遍历)
     * 如果是元素，需要解析指令
     * 如果是文本，需要解析插值表达式
     */
    this.toArray(fragment.childNodes).forEach(node => {
      // 如果是元素，需要解析指令
      if (this.isElementNode(node)) {
        this.compileNode(node)
      }
      // 如果是文本，需要解析插值表达式
      if (this.isTextNode(node)) {
        this.compileText(node)
      }
      // !!!敲黑板   如果当前节点有子节点的话，需要递归解析
      if (node.childNodes && node.childNodes.length > 0) {
        this.compile(node)
      }
    });
  }
  // 解析元素节点
  compileNode(node) {
    // console.log(node)
    // 1. 获取所有属性,attributes只显示在元素上程序员写的属性
    // console.log(node.attributes)
    let attributes = node.attributes
    // 2. 获取vue的指令（v开头）,获得具体的指令名，然后分析类型，得到具体值
    this.toArray(attributes).forEach(attr => {
      // attr是一个对象,不是string类型，不能直接startsWith
      let attrName = attr.name
      // console.dir(attr)
      if (this.isDirective(attrName)) {
        // expr这样在下面可能语义更好些
        let expr = attr.value
        // console.log(attrValue);
        let type = attrName.slice(2)
        // 这里的compileUtil是优化的
        if (this.isEventDirective(type)) {
          // v-on指令 比较特殊 需要特殊处理v-on:click="clickFn"
          compileUtil.eventHandle(node, this.vm, expr, type)
        } else {
          // 任何时候都记得判断属性存在才有后续
          compileUtil[type] && compileUtil[type](node, this.vm, expr)
        }

      }
    })

  }
  // 解析文本节点
  compileText(node) {
    // 这里也优化过
    compileUtil.mustache(node, this.vm)
  }


  /**
   * 工具方法
   */
  toArray(likeArray) {
    return [].slice.call(likeArray)
  }
  isElementNode(node) {
    // 1节点类型 3文本类型
    return node.nodeType === 1
  }
  isTextNode(node) {
    // 1节点类型 3文本类型
    return node.nodeType === 3
  }
  isDirective(attrName) {
    return attrName.startsWith('v-')
  }
  isEventDirective(attrName) {
    return attrName.split(':')[0] === 'on'
  }

}
/**
 *      // 单纯这样if太多选项，代码不易拓展，放到compileUtil那边  if (type === 'text') if (type === 'html') if (type === 'model')
        // v-text
        if (type === 'text') {
          console.log(this.vm.$data)
          node.textContent = this.vm.$data[expr]
        }
        // v-html
        if (type === 'html') {
          node.innerHTML = this.vm.$data[expr]
        }
        // v-model input元素
        if (type === 'model') {
          node.value = this.vm.$data[expr]
        }
        // v-on指令 比较特殊 需要特殊处理v-on:click="clickFn"
        if (this.isEventDirective(type)) {
          let eventType = type.split(':')[1]
          let eventFn = this.vm.$methods[expr].bind(this.vm)

          // 绑定事件之后，eventFn的this变成node本身，methods里的this应该指向vm实例，这里还需要把vm的data所有属性全部挂载到vm下，这个后续，目前获得vm的data就通过this.$data.属性,后期直接this.属性
          node.addEventListener(eventType, eventFn)
        }

        再继续，可以把编译的方法都放到这里来，比如文本节点之前是单独的
  compileText(node) {
    let textContent = node.textContent
    // 匹配{{msg}}插值表达式
    let reg = /\{\{(.+)\}\}/g
    // 只有插值表达式的才需要解析，找到值，相应的替换，注意这里是相应的替换，不是把整个textContent替换
    if(reg.test(textContent)){
      let expr = RegExp.$1
      console.log(RegExp)
      console.log(RegExp.$1)
      node.textContent = textContent.replace(reg,compileUtil.getVMValue(this.vm,expr))
      // node.textContent = textContent.replace(reg,function(){
      //   console.log(arguments)
      // })
    }
  }

 */
let compileUtil = {
  mustache(node, vm) {
    let textContent = node.textContent
    // 匹配{{msg}}插值表达式
    let reg = /\{\{(.+)\}\}/g
    // 只有插值表达式的才需要解析，找到值，相应的替换，注意这里是相应的替换，不是把整个textContent替换
    if (reg.test(textContent)) {
      let expr = RegExp.$1
      node.textContent = textContent.replace(reg, this.getVMValue(vm, expr))
      new Watch(vm, expr, (newValue) => {
        node.textContent = textContent.replace(reg, newValue)
      })
      // node.textContent = textContent.replace(reg,function(){
      //   console.log(arguments)
      // })
    }
  },
  // 优化if一般 switch 或者写成对象或者return 这个属于优化代码了
  text(node, vm, expr) {
    // 初次渲染
    node.textContent = this.getVMValue(vm, expr)
    // node.textContent = vm.$data[expr]
    // 如果expr的值发生变化就需要重新执行这个函数。
    // 通过watch对象监听expr数据变化，一旦变化，执行回调函数,重新渲染页面
    // 只要调用watch对象的update的时候才会执行cb
    /**
     * 同一个watch对象，一旦值发生变化，就执行watch的update,而update里面就会执行cb，也就是重新渲染页面
     */
    new Watch(vm, expr, (newValue, oldValue) => {
      debugger
      node.textContent = newValue
    })
  },
  html(node, vm, expr) {
    node.innerHTML = this.getVMValue(vm, expr)
    new Watch(vm, expr, newValue => {
      node.innerHTML = newValue
    })
  },
  model(node, vm, expr) {
    // let _this = this
    node.value = this.getVMValue(vm, expr)
    // 双向绑定,当输入的时候值也发生变化。
    node.addEventListener('input',()=>{
        // vm.$data[expr] = node.value
        this.setVMValue(vm, expr, node.value)
    })
    new Watch(vm, expr, newValue => {
      node.value = newValue
    })
  },
  eventHandle(node, vm, expr, type) {
    let eventType = type.split(':')[1]
    // && 都是在判断属性的存在，以防止undefined的报错
    let eventFn = vm.$methods && vm.$methods[expr]
    if (eventType && eventFn) {
      // 绑定事件之后，eventFn的this变成node本身，methods里的this应该指向vm实例，这里还需要把vm的data所有属性全部挂载到vm下，这个后续，目前获得vm的data就通过this.$data.属性,后期直接this.属性
      node.addEventListener(eventType, eventFn.bind(vm))
    }
  },
  // 根据表达式拿到vm对应的值 比如表达式是car.brand这里考虑到复杂的对象形式 this.vm.car.brand
  getVMValue(vm, expr) {
    let value = vm.$data
    expr.split('.').forEach(item => {
      value = value[item]
    })
    return value

  },
  // expr = car.color => data[car][color] 又用到迭代了
  // data[car][a]   data[car][a][b] = 9 这里特别注意最后一个的时候直接赋值，不是最后一个追加，犯错的点在于是最后一个还在追加
  setVMValue(vm, expr, value) {
    let key = vm.$data
    let arr = expr.split('.')
    arr.forEach((item, index) => {
      if (index < (arr.length - 1)) {
        key = key[item]
        console.log(key,'key')
      }else{
        key[item] = value
      }
    })
  }

}
/**
 * 1. 任何时候都记得判断属性存在才有后续,obj.attr && obj.attr.attr1
 * 1. if(varible==='a'){re(d,c)} if(varible==='b'){ree(d,c)} if(varible==='c'){reee(d,c)}可以优化成obj={a:re(d,c),b:ree(d,c),c:reee(d,c)}...
 * 1. Regexp.$1
 * 1. debugger
 * 1. reduce
 * 1. /\{\{(.+)\}\}/g不能识别{{msg}}{{tag}} 会识别成‘msg}}{{tag’，、修改建议别人的：正则表达式也应该修改，加上g修饰符并且将贪婪模式改成惰性模式 let reg=/\{\{(..*?)\}\}/g;node.textContent=txt.replace(reg,(a,b)=>{ new Watcher(vm,b,newValue =>{ node.textContent =txt.replace(reg,newValue) }) return this.getVMValue(vm,b) })
 * 1. 对于对象的读取和设置操作，需要考虑到对象里有对象的情况，所以不简单的写成obj[key]，而需要用函数。
 */